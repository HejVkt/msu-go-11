package main

import "fmt"

type job func(in, out chan interface{})

func Pipe(jobs ...job) {

	in := make(chan interface{})
	out := make(chan interface{})

	for _, j := range jobs {
		j(in, out)
	}

	return
}

func main() {

	res2 := ""

	case2 := []job{
		job(func(in, out chan interface{}) {
			for _, word := range []string{"Hello", "World"} {
				out <- word
			}
		}),
		job(func(in, out chan interface{}) {
			for word := range in {
				fmt.Println("Got", word)
				if w, ok := word.(string); ok {
					fmt.Println("And it is a string")
					if len(res2) > 0 {
						res2 = res2 + " " + w
					} else {
						res2 = w
					}
				}
			}
		}),
	}

	Pipe(case2...)

}
